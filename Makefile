.PHONY: run
run:
	go run --work main.go

.PHONY: build
build:
	go build -v main.go

.DEFAULT_GOAL := build