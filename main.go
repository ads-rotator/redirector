package main

import (
	"context"
	_ "database/sql"
	"flag"
	"github.com/gorilla/mux"
	_ "github.com/jackc/pgx"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/joho/godotenv"
	"log"
	"net/http"
	"os"
	"strconv"
)

var db *pgxpool.Pool

type PartnerCategory struct {
	ID     int    `db:"id"`
	Script string `db:"script"`
}

func Partner(w http.ResponseWriter, r *http.Request) {
	name := mux.Vars(r)["name"]
	var pc PartnerCategory
	err := db.QueryRow(context.Background(), "SELECT id, script FROM partners_category WHERE slug_name=$1", name).Scan(&pc.ID, &pc.Script)
	if err != nil {
		//log.Fatalln(err)
	} else {
		w.WriteHeader(http.StatusOK)
		if _, err := w.Write([]byte(pc.Script)); err != nil {
			//log.Fatalln(err)
		}
	}
	_, err = db.Exec(context.Background(), "UPDATE partners_partnerprogram set number_of_views=(number_of_views + 1) WHERE category_id=$1", pc.ID)
	if err != nil {
		//log.Fatalln(err)
	}
}

func PartnerRedirect(w http.ResponseWriter, r *http.Request) {
	pid := mux.Vars(r)["partner_id"]
	var link string
	err := db.QueryRow(context.Background(), "SELECT link FROM partners_partnerprogram WHERE id=$1", pid).Scan(&link)
	if err != nil {
		//log.Fatalln(err)
	} else {
		w.Header().Set("location", link)
		http.Redirect(w, r, link, http.StatusFound)
	}
}

func main() {
	err := godotenv.Load(os.ExpandEnv(".env"))
	if err != nil {
		log.Fatal("Error loading .env file", err)
	}
	hostPtr := flag.String("host", "localhost", "a string")
	portPtr := flag.Int("port", 9000, "an int")
	poolConfig, err := pgxpool.ParseConfig(os.Getenv("DATABASE_URL"))
	if err != nil {
		//log.Fatalln("Unable to parse config string", err)
	}

	db, err = pgxpool.ConnectConfig(context.Background(), poolConfig)
	if err != nil {
		//log.Fatalln("Unable to create connection pool", err)
	}

	router := mux.NewRouter()
	router.HandleFunc("/partners/{name}/", Partner).Methods("GET")
	router.HandleFunc("/partners/partner_redirect/{partner_id}", PartnerRedirect).Methods("GET")
	log.Println("Listening on PORT:", *portPtr)
	if err := http.ListenAndServe(*hostPtr+":"+strconv.Itoa(*portPtr), router); err != nil {
		//log.Fatalln(err)
	}
}
